<?php
session_destroy();

// очищаем куки
foreach (array_keys($_COOKIE) as $cookieName) {
  if (stristr($cookieName, '_value')) {
    setcookie($cookieName, '', 1);
  }
}

header('Location: ./');