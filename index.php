<?php
header('Content-Type: text/html; charset=UTF-8');
include('global_func.php');

// вспомогательный массив для суперспособностей
$skills_labels = [
  'immortality' => 'Immortality',
  'idclip' => 'Passing Through Walls',
  'fly' => 'Fly'
];

$popup_messages = array();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // наличие ошибок
  $errors = array();
  $messages = array();
  // массив с успешно сохраненными данными для автозаполнения
  $values = array();

  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и ранее в сессию записан факт успешного логина.
  // (если заходим после успешного логина)
  if (empty($errors) && isset($_COOKIE[session_name()]) && session_start() && isset($_SESSION['login'])) {
    // загружаем данные пользователя из БД и заполняем переменную $values
    $db = connectToDB();
    try {
      $stmt = $db->prepare("SELECT * FROM user5 WHERE login = ?");
      $stmt->execute([$_SESSION['login']]);
      $response = $stmt->fetch(PDO::FETCH_ASSOC);
      // print 'Response from db:</br>';
      // print_r($response);
      foreach ($response as $key => $value) {
        switch ($key) {
          case 'name':
            $values[$key] = $value;
            break;
          case 'email':
            $values[$key] = $value;
            break;
          case 'birthday':
            $values[$key] = $value;
            break;
          case 'sex':
            $values[$key] = $value;
            break;
          case 'limbs':
            $values[$key] = $value;
            break;
          case 'skill_immortality':
            if ($value == 1) {
              $values[substr($key, 6)] = $value;
            }
            break;
          case 'skill_idclip':
            if ($value == 1) {
              $values[substr($key, 6)] = $value;
            }
            break;
          case 'skill_fly':
            if ($value == 1) {
              $values[substr($key, 6)] = $value;
            }
            break;
          case 'biography':
            $values[$key] = $value;
            break;
        }
      }
    } catch (PDOException $e) {
      exit($e->getMessage());
    }
  } else {
    // если заходим не после успешного логина
    foreach (array_keys($_COOKIE) as $cookieName) {
      if (stristr($cookieName, '_value')) {
        $values[substr($cookieName, 0, -6)] = $_COOKIE[$cookieName];
      }
    }
  }
  // print('</br></br>VALUES</br>');
  // print_r($values);

  // если успешно сохранили данные
  if (isset($_COOKIE['save_form'])) {
    if ($_COOKIE['save_form'] == 1) {
      $popup_messages[] = 'Data was successfully send';
      // Если в куках есть пароль, то выводим сообщение.
      if (isset($_COOKIE['pass'])) {
        $reg_data_msg = sprintf(
          'You can <a href="login.php">log in</a> with:</br>
          login - <strong>%s</strong></br>
          password - <strong>%s</strong></br>',
          strip_tags($_COOKIE['login']),
          strip_tags($_COOKIE['pass'])
        );
      }

      // print('</br></br>POPUP MESSAGES</br>');
      // print_r($popup_messages);

      setcookie('login', '', 1);
      setcookie('pass', '', 1);
    } else if ($_COOKIE['save_form'] == 0) {
      // заполняем массив с сообщениями об ошибках и удаляем куки с ошибками
      foreach (array_keys($_COOKIE) as $cookieName) {
        if (stristr($cookieName, '_error')) {
          $messages[$cookieName] = $_COOKIE[$cookieName];
          setcookie($cookieName, '', 1);
        }
      }

      // поля с ошибками
      $errors = array_keys($messages);
    }
    // print('</br></br>MESSAGES</br>');
    // print_r($messages);
    // print('</br></br>ERRORS</br>');
    // print_r($errors);

    setcookie('save_form', '', 1); // удаляем куку о сохранении
  }

  // выводим форму
  include('form.php');
} else {
  // POST

  // VALIDATON
  $errors = false;

  // NAME
  if (empty($_POST['name'])) {
    setcookie('name_error', 'Fill the "Name"');
    $errors = true;
  } else if (!preg_match('/^[a-zA-Z, а-яА-Я]+$/u', $_POST['name'])) {
    setcookie('name_error', 'Use only Latin and Cyrillic characters');
    $errors = true;
  } else {
    // если корректно ввели, то сохраняем на год, чтобы потом вставлять, как базовое значение
    setcookie('name_value', $_POST['name'], time() + 365 * 24 * 60 * 60);
  }

  // EMAIL
  if (empty($_POST['email'])) {
    setcookie('email_error', 'Fill the "Email"');
    $errors = true;
  } else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    setcookie('email_error', 'Uncorrect Email');
    $errors = true;
  } else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
  }

  // BIRTHDAY
  if (empty($_POST['birthday'])) {
    setcookie('birthday_error', 'Pick your birthday');
    $errors = true;
  } else if (!(is_numeric($_POST['birthday']) && intval($_POST['birthday'] >= 1900 && intval($_POST['birthday']) <= 2020))) {
    setcookie('birthday_error', 'Birthday must be in interval 1900-2020');
    $errors = true;
  } else {
    setcookie('birthday_value', $_POST['birthday'], time() + 365 * 24 * 60 * 60);
  }

  // SEX
  if (empty($_POST['sex'])) {
    setcookie('sex_error', 'Pick your sex');
    $errors = true;
  } else if (!($_POST['sex'] === 'm' || $_POST['sex'] === 'f')) {
    setcookie('sex_error', 'Pick correct sex (m or f)');
    $errors = true;
  } else {
    setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
  }

  // LIMBS
  if (empty($_POST['limbs'])) {
    setcookie('limbs_error', 'Pick number of your limbs');
    $errors = true;
  } else if (!(is_numeric($_POST['limbs']) && intval($_POST['limbs']) >= 0 && intval($_POST['limbs']) <= 4)) {
    setcookie('limbs_error', 'Number of limbs must be in interval 0-4');
    $errors = true;
  } else {
    setcookie('limbs_value', $_POST['limbs'], time() + 365 * 24 * 60 * 60);
  }

  // SKILLS
  $skills_id = array_keys($skills_labels); // ['immortality','idclip', 'fly']
  if (empty($_POST['skills'])) {
    setcookie('skills_error', 'Pick skill(s)');
    $errors = true;
  } else {
    foreach ($_POST['skills'] as $skill) {
      if (!in_array($skill, $skills_id)) {
        setcookie('skills_error', 'Uncorrect skill: ' . $skill);
        $errors = true;
      } else {
        setcookie($skill . '_value', 1, time() + 365 * 24 * 60 * 60);
      }
    }
  }
  foreach ($skills_id as $skill) {
    $skill_insert[$skill] = in_array($skill, $_POST['skills']) ? 1 : 0;
  }

  // BIOGRAPHY
  if (strlen($_POST['biography']) < 200) {
    setcookie('biography_error', 'Biography have to include minimum 200 symbols');
    $errors = true;
  } else {
    setcookie('biography_value', $_POST['biography'], time() + 365 * 24 * 60 * 60);
  }

  // CONTRACT ACCEPT
  if (empty($_POST['contract_accept']) || $_POST['contract_accept'] != true) {
    setcookie('contract_accept_error', 'You must to accept the contract');
    $errors = true;
  }

  if ($errors) {
    // если есть ошибки, то перезагружаем страницу(там покажем сообщения об ошибках)
    setcookie('save_form', 0);
    header('Location: index.php');
    exit();
  } else {
    // удаляем куки с ошибками
    foreach (array_keys($_COOKIE) as $cookieName) {
      if (stristr($cookieName, '_error')) {
        setcookie($cookieName, '', 1);
      }
    }
  }

  // SAVE TO DB
  $db = connectToDB();

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (isset($_COOKIE[session_name()]) && session_start() && isset($_SESSION['login'])) {
    // одновляем данные пользователя в бд
    // TODO 0: проверять, какие данные надо обновить, а какие остались неизменными
    try {
      $stmt = $db->prepare("UPDATE user5 SET name = ?, email = ?, birthday = ?, sex = ?, limbs = ?,  skill_immortality = ?, skill_idclip = ?, skill_fly = ?, biography = ? WHERE login = ?");
      $stmt->execute([$_POST['name'], $_POST['email'], intval($_POST['birthday']), $_POST['sex'], intval($_POST['limbs']), $skill_insert['immortality'], $skill_insert['idclip'], $skill_insert['fly'], $_POST['biography'], $_SESSION['login'] ]);
    } catch (PDOException $e) {
      exit($e->getMessage());
    }
  } else {
    // Generate uniq login and pass
    // TODO 0: проверять, есть ли уже такие логин / пароль в базе
    $login = substr(md5(time()), 0, 8);
    $pass = substr(md5(time() + 1), 0, 8);
    $pass_hash = password_hash($pass, PASSWORD_DEFAULT);
    setcookie('login', $login);
    setcookie('pass', $pass);

    try {
      $stmt = $db->prepare("INSERT INTO user5 SET login = ?, pass_hash = ?, name = ?, email = ?, birthday = ?, sex = ?, limbs = ?,  skill_immortality = ?, skill_idclip = ?, skill_fly = ?, biography = ?");
      $stmt->execute([$login, $pass_hash, $_POST['name'], $_POST['email'], intval($_POST['birthday']), $_POST['sex'], intval($_POST['limbs']), $skill_insert['immortality'], $skill_insert['idclip'], $skill_insert['fly'], $_POST['biography']]);
    } catch (PDOException $e) {
      exit($e->getMessage());
    }
  }

  // кука об успешном сохранении
  setcookie('save_form', 1);

  header('Location: index.php');
}